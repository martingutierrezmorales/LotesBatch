@echo off
title Cerrar Programa - by BIZARRO
color 03
start hora.bat
mode con cols=75
mode con lines=5
:prog
cls
echo.
set /p variable2= Nombre exacto del Programa a cerrar :
:clock
echo.
set /p variable= Introduzca hora :
if %time:~0,5%:%time:~6,2% == %variable% (goto 1) else goto 2
:1
taskkill /s 127.0.0.1 /f /im %variable2%
goto ok
:2
cls
echo.
echo.
if %time:~0,5%:%time:~6,2% == %variable% (goto 1) else goto 2
goto 2
:ok
cls
echo.
echo Programa cerrado!!
sleep 5
goto prog
