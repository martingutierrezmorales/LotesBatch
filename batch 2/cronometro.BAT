@echo off
title * Hora *
color 0b
mode con cols=16
mode con lines=5
:clock
sleep 1
cls
echo.
echo %time:~0,5%:%time:~6,2%
echo.
echo %date%
goto clock

