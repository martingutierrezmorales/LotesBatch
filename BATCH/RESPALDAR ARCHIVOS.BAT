@ECHO OFF
@ECHO RESPALDO DE ARCHIVOS
REM creamos una variable conteniendo la fecha actual con el formato a�o-mes-dia
SET FOLDER=%date:~6,4%-%date:~3,2%-%date:~0,2%
REM Creamos la carpeta donde se guardar� la copia de respaldo
IF NOT EXIST Backup MKDIR Backup
MKDIR Backup%FOLDER%
REM Seteamos las propiedades del comando XCOPY
SET BACKUPCMD=XCOPY /S /C /D /E /H /I /R /Y
REM Ejecutamos la instrucci�n
%BACKUPCMD% Papelera reciclable. Backup%FOLDER%
PAUSE>NUL
